import matplotlib.pyplot as plt
import numpy

v = [-3, -2, -1, 0, 1]
w = [-5, -3, -1, 1, 3]
u = [1, 1, 1, 1, 1]

vPIu = 0
for i in range(len(v)):
    vPIu += v[i] * u[i]

uPIu = 0
for i in range(len(u)):
    uPIu += u[i] * u[i]

vPIv = 0
for i in range(len(v)):
    vPIv += v[i] * v[i]

uPIv = 0
for i in range(len(u)):
    uPIv += u[i] * v[i]

wPIu = 0
for i in range(len(w)):
    wPIu += w[i] * u[i]

wPIv = 0
for i in range(len(w)):
    wPIv += w[i] * v[i]

A = numpy.zeros((2, 2))
B = numpy.zeros((2, 1))

A[0][0] = vPIu
A[0][1] = uPIu
A[1][0] = vPIv
A[1][1] = uPIv

B[0] = wPIu
B[1] = wPIv

C = numpy.linalg.solve(A, B)

w_linha = C[0] * v + C[1] * u

plt.figure(figsize=(8, 8))
plt.plot(v, w, 'r.')
plt.plot(v, w_linha)
plt.show()

